#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 17:49:09 2019

@author: spiros
"""

import sys
import numpy as np
import pandas as pd
from my_functions import basu_barplots

dataset = sys.argv[1]

label = '_old_'+dataset+'_auc'
folder = 'data_finalSubmission/No_PCA'

fname1 = ['soma', 'dendrites']
fname2 = ['heatmaps']
fname3 = [ 'control', 'novel']

i = fname2[0]
algo='ANN'
pca='No_PCA'
filename_fig = 'figures/'+fname2[0]+'_'+algo+'_'+pca+'_'+dataset
means = []
stds  = []

for j in fname1:
    mean = []
    std = []
    for k in fname3:
        filename = folder+'_'+j+'_'+i+'_'+k+label+'.txt'
        DATA = np.loadtxt(filename)
        mean.append(np.mean(DATA))
        std.append(np.std(DATA))

        f = [np.mean(DATA), np.std(DATA), len(DATA)]
        df = pd.DataFrame(f)
        df = df.transpose()
        df.to_csv(folder+'_'+j+'_'+i+'_'+k+'.csv', index=False, header=False)

    means.append(mean)
    stds.append(std)

df1 = pd.DataFrame(means, columns=fname3, index=fname1)
df1.to_csv(folder+'_means_'+i+'.csv', index=False, header=False)

if dataset == 'smoothed':
    title ='Smoothed heatmaps'
else:
    title ='Raw heatmaps'

basu_barplots(means,
              stds,
              ['Familiar Env.', 'Novel Env.'],
              ['Chance Level']+['Soma', 'Dendrites'],
              title,
              filename_fig)

