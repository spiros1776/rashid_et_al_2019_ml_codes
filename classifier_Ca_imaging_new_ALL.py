#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 14:12:13 2019

@author: spiros
"""

import sys, os, pickle
import numpy as np

from my_functions import downsample

from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier

foldername = 'data_finalSubmission/'
if not os.path.exists(foldername+'codes/'):
    os.system('mkdir -p '+foldername+'codes/')
else:
    print('The data folder exists.')
os.system('cp *.py '+foldername+'codes/')


SOM_NOVEL = int(sys.argv[1])

if int(sys.argv[2])==0:
    c_basal  = False
    c_apical = False
elif int(sys.argv[2])==1:
    c_basal  = True
    c_apical = True

cj = (c_basal,c_apical)

if not cj[0] and not cj[1]:
    fname1='soma'
elif cj[0] and cj[1]:
    fname1='dendrites'

fname2='heatmaps'

if SOM_NOVEL==1:
    # Novel environment
    fname3='novel'
elif SOM_NOVEL==0:
    # Control conditions
    fname3='control'
else:
    sys.exit('Not a valid argument. Choose for Control:0, SOMdel:1, and Novel: 2')

namess = '_old_'+sys.argv[3]
fname = fname1+'_'+fname2+'_'+fname3+namess

with open('python_datasets/'+fname+'.pkl', 'rb') as f:
    data = pickle.load(f, encoding='latin1')

X = data['F']
y = data['y']

fname4='No_PCA'

fname_all = fname4+'_' + fname
print ('Current case simulated... ' + fname_all)


per = []
for i in range(100):
    if i%10==0:
        print (i)

    X_down, y_down = downsample(X, y, seed = i)

    X_train, X_test, y_train, y_test = train_test_split(X_down, y_down, test_size=0.2, stratify=y_down, random_state=i)

    classifier = MLPClassifier(hidden_layer_sizes=(200,100), activation='relu', 
                               solver='sgd', alpha=0.1, learning_rate='adaptive', learning_rate_init=0.2,
                               max_iter=1000, shuffle=True, early_stopping=True, validation_fraction=0.05,
                               batch_size=100)

    classifier.fit(X_train, y_train)

    y_pred = classifier.predict(X_test)

    per.append([metrics.f1_score(y_test, y_pred, 'weighted'), metrics.roc_auc_score(y_test, y_pred,average='weighted')])

per = np.array(per)
np.savetxt(foldername+fname_all+'_auc.txt', per[:,1])
np.savetxt(foldername+fname_all+'_f1_score.txt', per[:,0])

print ('\nFinished w/o errors... \n\n')
